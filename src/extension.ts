import * as vscode from 'vscode';
import * as ydl from 'youtube-dl';
let MPlayer = require('mplayer');
const y = require('youtube-dl');

var isDebug: boolean;
var isVerbose: boolean; 

let togglePauseButton: vscode.StatusBarItem;
let progressBar: vscode.StatusBarItem;
let titleLabel: vscode.StatusBarItem;
let isPause: boolean;

var playlistInfos: ydl.Info | undefined;

var player: any = undefined;
var currentSongDuration: number | undefined;

const playString = '\u25B6';
const pauseString = '\u23F8';
const progressBarString = "\u25AE";
const playlistUrl = "https://www.youtube.com/playlist?list=PLQC73oq6JtgyYNOeifrJXXzZ1-F0Kgmbg";
const debugPlaylistUrl = "https://www.youtube.com/playlist?list=PLQC73oq6JtgwlWLVesvKt6FbZi_NVGlBA";

export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(vscode.commands.registerCommand('extension.wboosterStart', () => {
		isDebug = vscode.workspace.getConfiguration('').get('isDebug', false);
		isVerbose = vscode.workspace.getConfiguration('').get('isVerbose', false);
		let infoUpdatedPromise = update_playlist_infos();
		infoUpdatedPromise.then(function(value) {
			playlistInfos = value;
			play_random_audio(playlistInfos);
		}, function(value){});
	}));

	const wboosterTogglePauseID = 'extension.wboosterTogglePause';
	context.subscriptions.push(vscode.commands.registerCommand(wboosterTogglePauseID, () => {
		isPause = !isPause;
		if (isPause === true){
			player.pause();
			togglePauseButton.text = playString;
		} else {
			player.play();
			togglePauseButton.text = pauseString;
		}
	}));

	isPause = false;
	togglePauseButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	togglePauseButton.command = wboosterTogglePauseID;
	togglePauseButton.text = pauseString;
	context.subscriptions.push(togglePauseButton);
	togglePauseButton.show();

	titleLabel = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 99);
	context.subscriptions.push(titleLabel);
	titleLabel.show();

	currentSongDuration = undefined;
	progressBar = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 98);
	update_progress_bar(undefined);
	context.subscriptions.push(togglePauseButton);
	progressBar.show();
}

function play_random_audio(playlistInfos: ydl.Info | undefined){
	if (!playlistInfos){
		return;
	}
	var infos: any = playlistInfos;
	var randomId = Math.floor(Math.random() * infos.length);
	let url = isDebug ? debugPlaylistUrl : playlistUrl;

	y.exec(
		url,
		[
			"--playlist-start", randomId + 1,
			"--playlist-end", randomId + 1,
			"-j"
		],
		{},
		function(err: any, output: string[]){
			let selectedVideoInfos = JSON.parse(output[0]);

			let title: string = selectedVideoInfos['title'];
			currentSongDuration = selectedVideoInfos['duration'];

			const outputPath = resolve_home_path(vscode.workspace.getConfiguration('').get('wboosterDirectory', '~/.wbooster/') + title + ".mp3");
			if (isVerbose){
				vscode.window.showInformationMessage("[WBooster] Now downloading : " + title);
			}
			y.exec(
				infos[randomId]['id'],
				[
					"--extract-audio",
					"--audio-format", "mp3",
					"-o", outputPath
				],
				{},
				function (err: any, output: string[]) {
					if (err) {
						vscode.window.showErrorMessage("[WBooster] Error downloading video audio");
						console.log(output);
						return;
					}
					player = new MPlayer();
					player.openFile(outputPath);
					player.play();
					player.volume(50);
					titleLabel.text = title;
					player.on('time', function (time: any) {
						update_progress_bar(time);
					});
					player.on('status', function (status: any) {
						if (status["position"] && status["filename"] === null) {
							currentSongDuration = undefined;
							play_random_audio(playlistInfos);
						}
					});
					if (isVerbose){
						vscode.window.showInformationMessage("[WBooster] Now playing : " + title);
					}
				}
			);

		}
	);
}

function update_playlist_infos(): Promise<ydl.Info>{
	if (isVerbose){
		vscode.window.showInformationMessage('[WBooster] Downloading playlist infos');
	}
	const y = require('youtube-dl');
	return new Promise(function(resolve, reject) {
		y.getInfo(isDebug ? debugPlaylistUrl : playlistUrl, ["--flat-playlist", "-j"], function (err: any, info: ydl.Info) {
			if (err) {
				vscode.window.showErrorMessage('[WBooster] Error downloading playlist infos');
				reject(undefined);
			}
			if (isVerbose) {
				vscode.window.showInformationMessage('[WBooster] Playlist infos downloaded !');
			}
			resolve(info);
		});
	});
}

function resolve_home_path(filepath: string) : string{
	const path = require('path');
	if (filepath[0] === '~') {
		return path.join(process.env.HOME, filepath.slice(1));
	}
	return filepath;
}

function update_progress_bar(songTime: number | undefined): void{
	if (!currentSongDuration || !songTime){
		progressBar.text = "[--------------------]";
		return;
	}
	let baseText = "[--------------------]";
	let progression = Math.round((songTime / currentSongDuration) * 20) + 1;
	progressBar.text = baseText.substring(0, progression) + progressBarString + baseText.substring(progression, baseText.length);
}

export function deactivate() {}
