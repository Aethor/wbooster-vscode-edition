*disclaimer : dont take anything here seriously*

*for the Zoo !*

# WBooster : VSCode Edition 

You wanted it, you begged for it, here it comes !

The already famous [WBooster](https://gitlab.com/Aethor/wbooster) productivity app, entirely rewritten for VSCode !


## Installation

* Go to the [Sourceforge project](https://sourceforge.net/projects/wbooster-vscode-edition) and download the lastest version
* Open VSCode, and search for *"Extensions: Install from VSIX..."* in the command palette. Select the wbooster VSIX file
* Install all external dependencies required :
    * mplayer
    * ffmpeg
    * youtube-dl

Everything is ready !


## Usage

to start the extension, use : 

>WBooster : start

Songs will be automatically played. A Play/Pause button is added to VSCode interface (in the left-bottom corner), as well as other minor UI components.

![Screenshot of the UI](assets/screenshot.png)

## Configuration

The following variables exist for configuration :

* wbooster.wboosterDirectory : song download directory
* wbooster.isVerbose 
* wbooster.isDebug


## FAQ

### You're bad at typescript

Yeah, I know.